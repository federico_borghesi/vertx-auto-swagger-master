package io.vertx.VertxAutoSwagger.Model;

import generator.Required;

public class AnchestorProduct {

    @Required
    private String anchestorId;


    public String getAnchestorId() {
        return anchestorId;
    }

    public void setAnchestorId(String anchestorId) {
        this.anchestorId = anchestorId;
    }
}
