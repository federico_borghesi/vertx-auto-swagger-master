package io.vertx.VertxAutoSwagger.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import generator.Required;

public class BaseProduct extends AnchestorProduct{

    @Required
    @JsonProperty(value = "BaseId", required = true)
    private String baseId;

    public BaseProduct() {
    }


    public String getBaseId(){
        return baseId;
    }

    public void setPeripheralId(String BaseId) {
        this.baseId = BaseId;
    }


}
