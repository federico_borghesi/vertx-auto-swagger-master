package generator;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.vertx.VertxAutoSwagger.Model.TestEnum;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ckaratza
 * Simple OpenApi Annotation mapping to OpenApi models. This part needs modifications to cover all spec.
 */
public final class AnnotationMappers {

    private static final Logger log = LoggerFactory.getLogger(AnnotationMappers.class);

    static void decorateOperationFromAnnotation(Operation annotation, io.swagger.v3.oas.models.Operation operation) {
        operation.summary(annotation.summary());
        operation.description(annotation.description());
        operation.operationId(annotation.operationId());
        operation.deprecated(annotation.deprecated());
        operation.setTags(Arrays.asList(annotation.tags()));

        //Mappa il request body
        if(annotation.requestBody().content().length !=0) {

            io.swagger.v3.oas.models.parameters.RequestBody rb = new io.swagger.v3.oas.models.parameters.RequestBody();

            Content content = GetContentFromAnnotation(annotation.requestBody().content()[0]);
            rb.setContent(content);
            rb.setRequired(annotation.requestBody().required());
            rb.description(annotation.requestBody().description());

            operation.requestBody(rb);
        }

        //Mappa le response
        ApiResponses apiResponses = new ApiResponses();
        apiResponses.putAll(
            Arrays.stream(annotation.responses()).map(response -> {
                ApiResponse apiResponse = new ApiResponse();
                apiResponse.description(response.description());
                if (response.content().length > 0) {
                    Arrays.stream(response.content()).forEach(content -> {
                        Content cont = GetContentFromAnnotation(content);
                        apiResponse.content(cont);
                    });
                }
                Arrays.stream(response.headers()).forEach(header -> {
                    Header h = new Header();
                    h.description(header.description());
                    h.deprecated(header.deprecated());
                    //h.allowEmptyValue(header.allowEmptyValue());
                    //Optional<Schema> schemaFromAnnotation = AnnotationsUtils.getSchemaFromAnnotation(header.schema());
                    //schemaFromAnnotation.ifPresent(h::schema);
                    h.required(header.required());
                    apiResponse.addHeaderObject(header.name(), h);
                });
                return new ImmutablePair<>(response.responseCode(), apiResponse);
            }).collect(Collectors.toMap(x -> x.left, x -> x.right)));
        operation.responses(apiResponses);

        //Mappa i parameter
        Arrays.stream(annotation.parameters()).forEach(parameter -> {
            Parameter p = findAlreadyProcessedParamFromVertxRoute(parameter.name(), operation.getParameters());
            if (p == null) {
                p = new Parameter();
                operation.addParametersItem(p);
            }
            p.name(parameter.name());
            p.description(parameter.description());
            p.allowEmptyValue(parameter.allowEmptyValue());
            try {
                p.style(Parameter.StyleEnum.valueOf(parameter.style().name()));
            } catch (IllegalArgumentException ie) {
                log.warn(ie.getMessage());
            }
            p.setRequired(parameter.required());
            p.in(parameter.in().name().toLowerCase());

            Schema schema = new Schema();
            io.swagger.v3.oas.annotations.media.Schema s = parameter.schema();
            if (!s.ref().isEmpty()) schema.set$ref(s.ref());
            schema.setDeprecated(s.deprecated());
            schema.setDescription(s.description());
            schema.setName(s.name());
            schema.setType(s.type());
            schema.setFormat(s.format());
            p.schema(schema);
        });
    }

    private static Content GetContentFromAnnotation(io.swagger.v3.oas.annotations.media.Content content) {

        Schema contentSchema;

        if(!content.array().schema().implementation().equals(Void.class)){
            //Map an array of a type
            io.swagger.v3.oas.annotations.media.ArraySchema arraySchemaAnnotation = content.array();
            Class innerType = arraySchemaAnnotation.schema().implementation();
            contentSchema= getArraySchemaFromInnerClass(innerType);
        } else {
            Class schemaAnnotation = content.schema().implementation();
            contentSchema= getSchemaFromAnnotation(schemaAnnotation);
        }

        Content cont = new Content().addMediaType("application/json", new MediaType().schema(contentSchema));
        return cont;

    }
    public static ArraySchema getArraySchemaFromInnerClass(Class annotatedArraySchemaClass) {

        ArraySchema schema = new ArraySchema();
        schema.setItems(getSchemaFromAnnotation(annotatedArraySchemaClass));
        return schema;
    }
    public static Schema getSchemaFromAnnotation(Class annotatedSchemaClass) {

        Schema schema = new Schema();
        //Get typpe of the class

        if(isPrimitiveOrWrapper(annotatedSchemaClass))
        {
            schema.setName(annotatedSchemaClass.getSimpleName());
            schema.setType(annotatedSchemaClass.getSimpleName());
        }
        else
        {
            schema.set$ref("#/components/schemas/" + annotatedSchemaClass.getSimpleName());
        }

        return schema;
    }
    private static Parameter findAlreadyProcessedParamFromVertxRoute(final String name, List<Parameter> parameters) {
        for (Parameter parameter : parameters) {
            if (name.equals(parameter.getName()))
                return parameter;
        }
        return null;
    }

    public static Boolean isPrimitiveOrWrapper(Type type){
        return type.equals(Double.class) ||
                type.equals(Float.class) ||
                type.equals(Long.class) ||
                type.equals(Integer.class) ||
                type.equals(Short.class) ||
                type.equals(Character.class) ||
                type.equals(Byte.class) ||
                type.equals(Boolean.class) ||
                type.equals(String.class);
    }

    public static Schema getModelSchemaFromField(Field field)   {
        Schema modelSchema;
        Class type = field.getType();
        Class componentType = field.getType().getComponentType();

        if(componentType!=null){
            modelSchema= getArraySchemaFromInnerClass(componentType);
        }
        else {
            modelSchema=getSchemaFromAnnotation(type);
        }

        return modelSchema;
    }

    public static ArrayList<Object> getValueListFromEnum(Class type)
    {
        ArrayList<Object> enumvalues=new ArrayList<>();


        for (Object obj : type.getEnumConstants())  {
            enumvalues.add(obj);
        }
       return enumvalues;
    }
}
